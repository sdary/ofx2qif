# OFX2QIF

OFX2QIF est un programme Python permettant de transformer un fichier OFX en fichier QIF.

## Installation

Le programme ne nécessite pas d'installation, il suffit de copier le répertoire OFX2QIF


## Usage

```bash
OFX2QIF fichiersource.ofx

```

Ce programme permet de retraiter un fichier OFX.
Le format OFX ne ferme pas systématiquement ses balises.
OFX2QIF est capable de fermer les balises suivantes :

- \<CODE>
- \<SEVERITY>
- \<DTSERVER>
- \<LANGUAGE>
- \<TRNUID>
- \<CLTCOOKIE>
- \<CURDEF>
- \<BANKID>
- \<BRANCHID>
- \<ACCTID>
- \<ACCTKEY>
- \<DTSTART>
- \<TRNTYPE>
- \<DTPOSTED>
- \<TRNAMT>
- \<FITID>
- \<NAME>
- \<BALAMT>
- \<DTASOF>
- \<TRNUID>
- \<CHECKNUM>
- \<DTEND>


Soit par exemple :

```xml

<STMTTRN><TRNTYPE>POS<DTPOSTED>20201106<TRNAMT>-69.10<FITID>PRXNG%LTKS<NAME>ACHAT CB VILLA MEDICIS  05.11.20</STMTTRN>
```

devient : 

```xml
D06/11/2020
T-69.10
PACHAT CB VILLA MEDICIS  05.11.20
MPOS
^

```

## Contributing

Les demandes d'amélioration sont les bienvenues. Pour les changements majeurs, veuillez d'abord ouvrir un numéro pour discuter de ce que vous souhaitez changer.


## License
[GNU AGPLv3](https://choosealicense.com/licenses/agpl-3.0/)