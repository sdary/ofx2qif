#!/usr/bin/python
# -*- coding: utf-8 -*-
# Python 3
#https://python.jpvweb.com/python/mesrecettespython/doku.php?id=cx_freeze
#compilation : python setup.py build


from cx_Freeze import setup, Executable

exe=Executable(
        script="OFX2QIF.py",
        #base="Win32Gui",                                    #Appli Windows avec fenetre
        #icon="iconmonstr-database-16-240.ico",              #Icone du fichier .exe
        targetName="OFX2QIF",                            #Non du fichier .exe
        copyright="Stéphane Dary"                                #Copyright du fichier
        )
includefiles=[]
includes=[]
excludes=["asyncio","concurrent","ctypes","distutils","email","html","http","lib2to3","multiprocessing","pydoc_data","test","unittest","urllib","xml","xmlrpc"]
packages=[]

setup(
        name = "OFX2QIF.exe",
        version = "0.1.0",
        description = "Outil de transformation de fichier OFX en QIF",
        author = "Stéphane Dary",
        options = {'build_exe': {'excludes':excludes,'packages':packages,'include_files':includefiles}},
        executables = [exe]
        )
