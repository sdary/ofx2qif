#! /usr/bin/python3
# -*- coding: utf-8 -*-
'''
Name ........ : OFX2QIF.py
Role ........ : Transformation d'un fichier OFX en QIF
Author ...... : Stéphane Dary
Version ..... : V0.1 07/11/2020
Licence ..... : GPL

'''
import sys
import os
from lxml import etree

try:
    fichiersource=sys.argv[1]
except IndexError:
    print('Pas d''argument, utilisation du fichier LBP.ofx en source')
    fichiersource="LBP.ofx"



#Traitement du fichier initial
#fichiersource="LBP.ofx"
#fichiersource="OFX2QIF.xml" #Test
#fichiersortie='LBP.QIF'

try:
    with open(fichiersource): pass
except IOError:
    print("Erreur! Le fichier "+fichiersource+" n'a pas pu être ouvert")
    sys.exit()



try:
    arbre = etree.parse(fichiersource)
except etree.XMLSyntaxError as err:
     print('Fichier non XML')
     fichiertmp='OFX2QIF.xml'
     filetmp=open(fichiertmp,'w')
     filetmp.close()
     filetmp=open(fichiertmp,'a',newline='')
     filetmp.write('<?xml version="1.0" encoding="utf-8" ?><?OFX OFXHEADER="100" VERSION="102" SECURITY="NONE" OLDFILEUID="NONE" NEWFILEUID="NONE"?>\n')
     file = open(fichiersource, "r")
     line=file.readline()
     while line:
        if line.find("<") == -1:
            line=file.readline()
        else:
            #remplace de <SEVERITY> par </CODE><SEVERITY>
            line=line.replace('<SEVERITY>', '</CODE>'+'\n'+'<SEVERITY>')

            #remplace de </STATUS> par </SEVERITY></STATUS>
            line=line.replace('</STATUS>', '</SEVERITY>'+'\n'+'</STATUS>')

            # <LANGUAGE> par </DTSERVER><LANGUAGE>
            line=line.replace('<LANGUAGE>', '</DTSERVER>'+'\n'+'<LANGUAGE>')

            # </SONRS> par </LANGUAGE></SONRS>
            line=line.replace('</SONRS>', '</LANGUAGE>'+'\n'+'</SONRS>')

            #<STATUS> par </TRNUID><STATUS>
            line=line.replace('<STATUS>', '</TRNUID>'+'\n'+'<STATUS>')

            #<STMTRS> par </CLTCOOKIE><STMTRS>
            line=line.replace('<STMTRS>', '</CLTCOOKIE>'+'\n'+'<STMTRS>')

            #<BANKACCTFROM> par </CURDEF><BANKACCTFROM>
            line=line.replace('<BANKACCTFROM>', '</CURDEF>'+'\n'+'<BANKACCTFROM>')

            #<BRANCHID> par </BANKID><BRANCHID>
            line=line.replace('<BRANCHID>', '</BANKID>'+'\n'+'<BRANCHID>')
            #<ACCTID> par <BRANCHID><ACCTID>
            line=line.replace('<ACCTID>', '</BRANCHID>'+'\n'+'<ACCTID>')
            #<ACCTTYPE> par </ACCTID><ACCTTYPE>
            line=line.replace('<ACCTTYPE>', '</ACCTID>'+'\n'+'<ACCTTYPE>')
            #<ACCTKEY> par </ACCTTYPE><ACCTKEY>
            line=line.replace('<ACCTKEY>', '</ACCTTYPE>'+'\n'+'<ACCTKEY>')
            #</BANKACCTFROM> par </ACCTKEY></BANKACCTFROM>
            line=line.replace('</BANKACCTFROM>', '</ACCTKEY>'+'\n'+'</BANKACCTFROM>')
            #<DTEND> par </DTSTART><DTEND>
            line=line.replace('<DTEND>', '</DTSTART>'+'\n'+'<DTEND>')

            #<DTPOSTED> par </TRNTYPE><DTPOSTED> 
            line=line.replace('<DTPOSTED>', '</TRNTYPE>'+'\n'+'<DTPOSTED>')
            #<TRNAMT> par </DTPOSTED><TRNAMT>
            line=line.replace('<TRNAMT>', '</DTPOSTED>'+'\n'+'<TRNAMT>')
            #<FITID> par </TRNAMT><FITID>
            line=line.replace('<FITID>', '</TRNAMT>'+'\n'+'<FITID>')
            #<NAME> par </FITID><NAME>
            line=line.replace('<NAME>', '</FITID>'+'\n'+'<NAME>')
            line=line.replace('</STMTTRN>', '</NAME>'+'\n'+'</STMTTRN>'+'\n')
            line=line.replace('<STMTTRN><TRNTYPE>', '<STMTTRN>'+'\n'+'<TRNTYPE>')
            line=line.replace('<DTASOF>', '</BALAMT>'+'\n'+'<DTASOF>')
            line=line.replace('</LEDGERBAL>', '</DTASOF>'+'\n'+'</LEDGERBAL>'+'\n')
            line=line.replace('</AVAILBAL>', '</DTASOF>'+'\n'+'</AVAILBAL>'+'\n')
            line=line.replace('<SONRS></TRNUID>', '<SONRS>')
            #Gestion des balises CHECKNUM optionnelles
            pos_end = 0
            while True:
                pos_begin = line.find('<CHECKNUM>',pos_end)
                pos_end = line.find('</FITID>',pos_begin)
                #print (str(pos_begin)+'-'+str(pos_end))
                if pos_begin == -1:
                    break
                extract=line[pos_begin:pos_end]
                line=line.replace(extract,extract+'</CHECKNUM>'+'\n')
                #print(extract+'-'+extract+'</CHECKNUM>'+'\n')


            #Gestion des balises DTEND unique
            pos_end = 0
            while True:
                pos_begin = line.find('<DTEND>',pos_end)
                pos_end = line.find('<STMTTRN>',pos_begin)
                #print (str(pos_begin)+'-'+str(pos_end))
                if pos_begin == -1:
                    break
                extract=line[pos_begin:pos_end]
                line=line.replace(extract,extract+'</DTEND>'+'\n')
                #print(extract+'-'+extract+'</DTEND>'+'\n')                

            filetmp.write(line+'\n')
            line=file.readline()
     filetmp.close()
     fichiersource='OFX2QIF.xml'
     #print(fichier)


finally:
    print('Fichier Source : Ok')
    # print(filework)

parser = etree.XMLParser(remove_blank_text=True)
arbre = etree.parse(fichiersource, parser)


fileout=open('LBP.QIF','w')
fileout.write('!Type:Bank'+'\n')
#print()
parser = etree.XMLParser(remove_blank_text=True)
arbre = etree.parse(fichiersource, parser)

for noeud in arbre.xpath('/OFX/BANKMSGSRSV1/STMTTRNRS/STMTRS/BANKTRANLIST/STMTTRN'):
    for STMTTRN in noeud.iter('STMTTRN'):

        #Traitement de la date (D-DTPOSTED)
        DATE=STMTTRN.xpath("DTPOSTED")[0].text
        #print(DATE)
        DATE='D'+DATE[6:8]+'/'+DATE[4:6]+'/'+DATE[0:4]
        #print(DATE)
        fileout.write(DATE+'\n')

        #Traitement du montant (T-TRNAMT)
        MT='T'+STMTTRN.xpath("TRNAMT")[0].text
        #print(MT)
        fileout.write(MT+'\n')

        #Traitement du Nom (P-NAME)
        NOM='P'+STMTTRN.xpath("NAME")[0].text
        #print(NOM)
        fileout.write(NOM+'\n')

        #Traitement du Nom (MEMO)
        MEMO='M'#+STMTTRN.xpath("MEMO")[0].text
        MEMO=MEMO+STMTTRN.xpath("TRNTYPE")[0].text
        try:
            MEMO=MEMO+' '+STMTTRN.xpath("MEMO")[0].text
        except IndexError:
            #print("Oops!  il n'y a pas de mémo")        
            #print(MEMO)
            fileout.write(MEMO+'\n')

        #Traitement du caractère séparateur d'enregistrement
        #print('^')
        fileout.write('^'+'\n')
fileout.close
print('Génération du fichier : '+'LBP.QIF')
